# Task assessor
[![Build Status](https://travis-ci.org/mikron-ia/asesor.svg?branch=master)](https://travis-ci.org/mikron-ia/asesor)
[![Code Climate](https://codeclimate.com/github/mikron-ia/asesor/badges/gpa.svg)](https://codeclimate.com/github/mikron-ia/asesor)
[![Test Coverage](https://codeclimate.com/github/mikron-ia/asesor/badges/coverage.svg)](https://codeclimate.com/github/mikron-ia/asesor/coverage)

Web application to appraise tasks according to given general rules.

## Background
The application was created to appraise and evaluate tasks presented to player characters in Eclipse Phase story. 
In universe, this task falls to so-called muses - personal AI assistants - who give their opinion based on their 
personalities and experiences. 

To date, I performed the evaluations manually, which was rather tedious (especially if several points of view for
several tasks were required). This project is an attempt on automation of the task, along with my attempt to better
undestand DDD. Being aware of [automation backfire phenomenon](https://xkcd.com/1319/), I cannot promise this project
will be maintained beyond basic functionality (once it is achieved, which is not the case yet).

## Development
### Required for v0.1
* Minimal viable domain model, test covered
* Basic data loading
* Basic data processing
* Basic presentation

### Required for v0.2
* Reasonable presentation
* Presentation and dependency injection done properly via lean framework (likely Silex) 

## Installation guide
... will be created once the project does anything practical.